import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { TransactionSingleComponent } from './transaction-single/transaction-single.component';
import { NewsSingleComponent } from './news-single/news-single.component';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent
  },
  {
    path: 'transaction/:id',
    component: TransactionSingleComponent
  },
  {
    path: 'news/:id',
    component: NewsSingleComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
