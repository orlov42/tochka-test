import { Injectable } from '@angular/core';

@Injectable()
export class DataService {

  nextId = 8;

  transactions = [
    {
      id: '1',
      amount: '10041',
      currency: '$',
      description: 'Описание транзакции',
      from: 'Maxim',
      date: '13.10.2018',
      direction: 'incoming',
      type: 'type'
    },
    {
      id: '2',
      amount: '1337',
      currency: 'руб',
      description: 'Описание транзакции',
      from: 'Petya',
      date: '14.10.2018',
      direction: 'outcoming',
      type: 'type'
    },
    {
      id: '3',
      amount: '77781',
      currency: 'руб',
      description: 'Описание транзакции',
      from: 'Sberbank',
      date: '15.10.2018',
      direction: 'incoming',
      type: 'type'
    },
    {
      id: '4',
      amount: '77781',
      currency: 'руб',
      description: 'Описание транзакции',
      from: 'Sberbank',
      date: '16.10.2018',
      direction: 'incoming',
      type: 'type'
    },
    {
      id: '5',
      amount: '77781',
      currency: 'руб',
      description: 'Описание транзакции',
      from: 'Sberbank',
      date: '17.10.2018',
      direction: 'incoming',
      type: 'type'
    },
    {
      id: '6',
      amount: '77781',
      currency: 'руб',
      description: 'Описание транзакции',
      from: 'Sberbank',
      date: '18.10.2018',
      direction: 'incoming',
      type: 'type'
    },
    {
      id: '7',
      amount: '77781',
      currency: 'руб',
      description: 'Описание транзакции',
      from: 'Sberbank',
      date: '19.10.2018',
      direction: 'incoming',
      type: 'type'
    }
  ];

  news = [
    {
      id: '1',
      read: false,
      title: 'Тестовая новость №1',
      date: '12.12.2012',
      content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi at sapien eros. Nulla quis pulvinar eros, sit amet dignissim nunc. Nam rhoncus lacus non felis lobortis, id condimentum ex aliquam. Aliquam nulla nibh, rutrum ac ex et, porttitor posuere magna. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas sed nisl suscipit quam vestibulum imperdiet vitae a est. Sed pulvinar neque in sagittis varius. Fusce in aliquet felis, sit amet interdum nulla. Ut eleifend, nisi at dignissim viverra, odio urna mattis leo, ac iaculis enim felis vitae neque. Cras ornare dui eu lorem rutrum pulvinar. Nulla sed ante eget ligula euismod mattis. Nullam fermentum, velit a finibus laoreet, erat nulla commodo lacus, sit amet lobortis eros risus ac enim. Nulla sit amet nibh eget nisl finibus feugiat in et urna. In ac purus malesuada, posuere urna eu, fermentum est. Integer in nisl laoreet, sagittis tellus ut, ultricies purus. Fusce pharetra varius dapibus.'
    },
    {
      id: '2',
      date: '13.13.2013',
      read: false,
      title: 'Тестовая новость №2',
      content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi at sapien eros. Nulla quis pulvinar eros, sit amet dignissim nunc. Nam rhoncus lacus non felis lobortis, id condimentum ex aliquam. Aliquam nulla nibh, rutrum ac ex et, porttitor posuere magna. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas sed nisl suscipit quam vestibulum imperdiet vitae a est. Sed pulvinar neque in sagittis varius. Fusce in aliquet felis, sit amet interdum nulla. Ut eleifend, nisi at dignissim viverra, odio urna mattis leo, ac iaculis enim felis vitae neque. Cras ornare dui eu lorem rutrum pulvinar. Nulla sed ante eget ligula euismod mattis. Nullam fermentum, velit a finibus laoreet, erat nulla commodo lacus, sit amet lobortis eros risus ac enim. Nulla sit amet nibh eget nisl finibus feugiat in et urna. In ac purus malesuada, posuere urna eu, fermentum est. Integer in nisl laoreet, sagittis tellus ut, ultricies purus. Fusce pharetra varius dapibus.'
    },
    {
      id: '3',
      read: true,
      date: '14.14.2014',
      title: 'Тестовая новость №3',
      content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi at sapien eros. Nulla quis pulvinar eros, sit amet dignissim nunc. Nam rhoncus lacus non felis lobortis, id condimentum ex aliquam. Aliquam nulla nibh, rutrum ac ex et, porttitor posuere magna. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas sed nisl suscipit quam vestibulum imperdiet vitae a est. Sed pulvinar neque in sagittis varius. Fusce in aliquet felis, sit amet interdum nulla. Ut eleifend, nisi at dignissim viverra, odio urna mattis leo, ac iaculis enim felis vitae neque. Cras ornare dui eu lorem rutrum pulvinar. Nulla sed ante eget ligula euismod mattis. Nullam fermentum, velit a finibus laoreet, erat nulla commodo lacus, sit amet lobortis eros risus ac enim. Nulla sit amet nibh eget nisl finibus feugiat in et urna. In ac purus malesuada, posuere urna eu, fermentum est. Integer in nisl laoreet, sagittis tellus ut, ultricies purus. Fusce pharetra varius dapibus.'
    },
    {
      id: '4',
      read: false,
      date: '13.13.2013',
      title: 'Тестовая новость №4',
      content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi at sapien eros. Nulla quis pulvinar eros, sit amet dignissim nunc. Nam rhoncus lacus non felis lobortis, id condimentum ex aliquam. Aliquam nulla nibh, rutrum ac ex et, porttitor posuere magna. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas sed nisl suscipit quam vestibulum imperdiet vitae a est. Sed pulvinar neque in sagittis varius. Fusce in aliquet felis, sit amet interdum nulla. Ut eleifend, nisi at dignissim viverra, odio urna mattis leo, ac iaculis enim felis vitae neque. Cras ornare dui eu lorem rutrum pulvinar. Nulla sed ante eget ligula euismod mattis. Nullam fermentum, velit a finibus laoreet, erat nulla commodo lacus, sit amet lobortis eros risus ac enim. Nulla sit amet nibh eget nisl finibus feugiat in et urna. In ac purus malesuada, posuere urna eu, fermentum est. Integer in nisl laoreet, sagittis tellus ut, ultricies purus. Fusce pharetra varius dapibus.'
    },
    {
      id: '5',
      read: false,
      date: '15.13.2013',
      title: 'Тестовая новость №5',
      content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi at sapien eros. Nulla quis pulvinar eros, sit amet dignissim nunc. Nam rhoncus lacus non felis lobortis, id condimentum ex aliquam. Aliquam nulla nibh, rutrum ac ex et, porttitor posuere magna. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas sed nisl suscipit quam vestibulum imperdiet vitae a est. Sed pulvinar neque in sagittis varius. Fusce in aliquet felis, sit amet interdum nulla. Ut eleifend, nisi at dignissim viverra, odio urna mattis leo, ac iaculis enim felis vitae neque. Cras ornare dui eu lorem rutrum pulvinar. Nulla sed ante eget ligula euismod mattis. Nullam fermentum, velit a finibus laoreet, erat nulla commodo lacus, sit amet lobortis eros risus ac enim. Nulla sit amet nibh eget nisl finibus feugiat in et urna. In ac purus malesuada, posuere urna eu, fermentum est. Integer in nisl laoreet, sagittis tellus ut, ultricies purus. Fusce pharetra varius dapibus.'
    }
  ];

  constructor() {
  }

  createTransaction(params) {
    params['id'] = this.nextId;
    this.transactions.push(params);
    this.nextId++;
  }

  deleteTransaction(id) {
    const idx = this.findTransaction(id);

    this.transactions = [
      ...this.transactions.slice(0, idx),
      ...this.transactions.slice(idx + 1)
    ];
  }

  findTransaction(id) {
    return this.transactions.findIndex((item) => {
      return item.id == id;
    });
  }

  findNews(id) {
    return this.news.findIndex((item) => {
      return item.id == id;
    });
  }

  toggleNewsRead(id) {
    const attr = this.news[this.findNews(id)].read;
    this.news[this.findNews(id)].read = !attr;
  }

}
