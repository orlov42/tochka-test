import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { TransactionListComponent } from './transaction-list/transaction-list.component';
import { TransactionSingleComponent } from './transaction-single/transaction-single.component';
import { DataService } from './data.service';
import { FormsModule } from '@angular/forms';
import { ModalComponent } from './modal/modal.component';
import { NewsComponent } from './news/news.component';
import { NewsSingleComponent } from './news-single/news-single.component';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    TransactionListComponent,
    TransactionSingleComponent,
    ModalComponent,
    NewsComponent,
    NewsSingleComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [
    DataService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
