import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  template: `          
        <app-transaction-list></app-transaction-list>
        <app-news></app-news>
    `,
  styles: []
})
export class HomeComponent {}
