import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';

@Component({
  selector: 'app-news',
  template: `
      <h1 class="text-center">
          Новости
      </h1>
      <div class="news">
          <div *ngFor="let news of allNews" [class]="(news.read) ? 'news-item read': 'news-item'">
              <div class="header">
                  <h2> {{news.title}}</h2>
              </div>
              <div class="footer">
                  <button class="btn" [routerLink]="['/news/'+news.id]">Подробнее</button>
              </div>
          </div>
      </div>

  `,
  styles: [
      `
          h1 {
              margin-top: 50px;
          }

          .news {
              display: flex;
              width: 80%;
              margin: auto;
              justify-content: flex-start;
              flex-wrap: wrap;
              align-items: center;
          }

          .news-item {
              width: 15%;
              margin: 20px auto;
              border: 1px solid black;
              padding: 30px;
              border-radius: 5px;
          }

          .news-item.read {
              background: #c2ffb9;
          }

          .news-item .header {
              text-align: center;
          }
    `
  ]
})
export class NewsComponent implements OnInit {
  allNews;

  constructor(
    private dataService: DataService
  ) {
  }

  ngOnInit() {
    this.allNews = this.dataService.news;
  }

}
