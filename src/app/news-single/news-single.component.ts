import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-news-single',
  template: `
      <div class="container">
          <div class="navbar"><a routerLink="">На главную</a></div>
          <div class="header">
              <span class="right-floated date">{{news.date}}</span>
              <h1>{{news.title}}</h1>
          </div>
          <div class="content">
              {{news.content}}
          </div>
          <div class="footer">
              <button class="btn" (click)="toggleRead()">
                  {{(news.read) ? "Забыть" : "Прочитать"}}
              </button>
          </div>
      </div>

  `,
  styles: [
      `
          .container {
              max-width: 50%;
              margin: auto;
          }

          .header h1 {
          }

          .date {
              font-style: italic;
              font-size: 18px
          }

          .right-floated {
              float: right;
          }
    `
  ]
})
export class NewsSingleComponent implements OnInit {
  news;

  constructor(
    private dataService: DataService,
    private route: ActivatedRoute
  ) {
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      const newsId = params['id'];
      const indexInArray = this.dataService.findNews(newsId);
      this.news = this.dataService.news[indexInArray];
      console.log(this.news);
    });
  }

  toggleRead() {
    this.dataService.toggleNewsRead(this.news.id);
  }

}
