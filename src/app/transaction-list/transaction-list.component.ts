import { Component, OnInit } from '@angular/core';
import { DataService } from './../data.service';


@Component({
  selector: 'app-transaction-list',
  template: `
      <h1 class="text-center">Список транзакций</h1>
      <table>
          <thead>
          <th></th>
          <th>Дата</th>
          <th>Сумма перевода</th>

          <th>Отправитель</th>
          </thead>
          <tbody>
          <tr *ngFor="let transaction of transactions"
              [routerLink]="['/transaction/'+transaction.id]"
              [class]="transaction.direction ==='incoming' ? 'link green' : 'link red'">
              <td>{{transaction.direction === 'incoming' ? '(+)' : '(-)'}}</td>
              <td>{{transaction.date}}</td>
              <td>{{transaction.amount}} {{transaction.currency}}</td>
              <td>{{transaction.from}}</td>
          </tr>
          </tbody>
      </table>

      <app-modal [modalId]="'createTransaction'"></app-modal>
  `,

  styles: [
    `
            .link {
                cursor: pointer;
            }

            .link:hover {
                background: aliceblue;
            }

            td {
                padding: 5px;
                border-bottom: 1px solid black;
                border-spacing: 5px;
            }

            table {
                width: 80%;
                margin: auto;
                text-align: center;
                
            }

            table th, table tr {

            }

            table th {
                font-weight: 500;
                background: #d7d7d7;
            }

            tr.green td {
                background-color: #88eb87;
            }

            tr.green:hover td {
                background-color: #76d775;
            }

            tr.red td {
                background-color: #ff877c;
            }

            tr.red:hover td {
                background-color: #e18888;
            }

        `
  ]
})
export class TransactionListComponent implements OnInit {
  transactions;

  constructor(
    private dataService: DataService
  ) {

  }

  ngOnInit() {
    this.transactions = this.dataService.transactions;
  }


}
