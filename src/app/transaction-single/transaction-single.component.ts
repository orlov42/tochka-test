import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DataService } from './../data.service';

@Component({
  selector: 'app-transaction-single',
  template: `
      <!--Заголовок-->
      <div class="navbar"><a routerLink="">На главную</a></div>
      <div class="container">
          <div class="card">
              <h1 class="card-title">
                  Информация по транзакции №{{transaction.id}}
              </h1>
              <div class="card-content">
                  <div class="field">Операция: <span>{{(transaction.direction === "incoming") ? "+" : "-"}}</span>
                  </div>
                  <div class="field">Сумма: <span>{{transaction.amount}} {{transaction.currency}}</span></div>
                  <div class="field">Отправитель: <span>{{transaction.from}}</span></div>
                  <div class="field">Дата: <span>{{transaction.date}}</span></div>
                  <div class="field">Описание: <span>{{transaction.description}}</span></div>
                  <div class="field">Тип операции: <span>{{transaction.type}}</span></div>

                  <button class="btn" (click)="deleteTransaction()"> Удалить транзакцию</button>

              </div>
          </div>
      </div>
  `,
  styles: [`
        .card-title {
            font-weight: 500;
        }

        .container {
            max-width: 1100px;
            margin: auto;
            display: flex;
            height: 500px;
            justify-content: space-around;

        }

        .card {
            border-left: 1px solid #c1c1c1;
            border-right: 1px solid #c1c1c1;
            padding: 10px 20px;
            width: 80%;
            margin: auto
        }

        .card-title {
            text-align: center;
            border-bottom: 1px solid #c1c1c1;
            padding: 10px;
            width: fit-content;
            margin: 20px auto;
        }

        .card-content {
            width: 80%;
            margin: auto;
            border-spacing: unset;
            border-collapse: collapse;
            text-align: center
        }

        .card-content .field {
            display: block;
            margin: 10px
        }

        .card-content span {
            font-size: 1.3rem;
            font-weigh: 500;
        }
    `]
})
export class TransactionSingleComponent implements OnInit {

  transaction;

  constructor(
    private route: ActivatedRoute,
    private dataService: DataService,
    private router: Router
  ) {
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      const transactionId = params['id'];
      const indexInArray = this.dataService.findTransaction(transactionId);
      this.transaction = this.dataService.transactions[indexInArray];
    });
  }

  deleteTransaction() {
    if (confirm('Уверены что хотите удалить эту транзакцию?')) {
      this.dataService.deleteTransaction(this.transaction.id);
      console.log(this.dataService.transactions);
      this.router.navigate(['']);
    }
  }
}
