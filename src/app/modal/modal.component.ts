import { Component, Input, OnInit } from '@angular/core';
import { DataService } from '../data.service';

@Component({
  selector: 'app-modal',
  template: `
        <button (click)="showModal()" class="btn">Добавить транзакцию</button>

        <div class="modal" [id]="modalId">
            <!-- Modal content -->
            <div class="modal-content">
                <span class="close" (click)="closeModal()">&times;</span>
                <form action="" (ngSubmit)="submitForm()" #addForm="ngForm">
                    <div class="field">
                        <label for="direction">Операция:</label>
                        <div class="select">
                            <select name="direction" id="direction" [(ngModel)]="direction" required>
                                <option value="incoming">Приход</option>
                                <option value="outcoming">Расход</option>
                            </select>
                        </div>
                       
                    </div>
                    <div class="field">
                        <label for="currency">Валюта:</label>
                        <div class="select">
                            <select name="currency" id="currency" [(ngModel)]="currency" required>
                                <option value="RUB">RUB</option>
                                <option value="USD">USD</option>
                                <option value="EUR">EUR</option>
                            </select>
                        </div>
                    </div>
                    <div class="field">
                        <label for="amount">Сумма транзакции:</label>
                        <input type="text" id="amount" name="amount" [(ngModel)]="amount" required>
                    </div>
                    
                    <div class="field">
                        <label for="from">Отправитель:</label>
                        <input id="from" name="from" type="text" [(ngModel)]="from" required>
                    </div>
                    <div class="field">
                        <label for="description">Описание:</label>
                        <textarea id="description" name="description" [(ngModel)]="description"></textarea>
                    </div>

                    <div class="field">
                        <label for="date">Дата:</label>
                        <input id="date" name="date" type="text" [(ngModel)]="date" required>
                    </div>
                    <div class="field">
                        <label for="type">Тип операции:</label>
                        <input id="type" name="type" type="text" [(ngModel)]="type" required>
                    </div>

                    <button type="submit" class="btn" [disabled]="addForm.invalid">Добавить</button>
                </form>
            </div>
        </div>
    `,
  styles: [
    `
            .modal {
                display: none;
                position: fixed;
                z-index: 1;
                left: 0;
                top: 0;
                width: 100%;
                height: 100%;
                overflow: auto;
                background-color: rgb(0,0,0);
                background-color: rgba(0,0,0,0.4);
            }
            .modal-content {
                background-color: #fefefe;
                margin: 5% auto;
                padding: 20px;
                border: 1px solid #888;
                width: 30%;
                box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);

            }
            .close {
                color: #aaa;
                float: right;
                font-size: 28px;
                font-weight: bold;
            }
            .close:hover,
            .close:focus {
                color: black;
                text-decoration: none;
                cursor: pointer;
            }
            
            .field{
                margin-bottom: .75rem;
                width: 70%;
            }
            .field label{
                margin-bottom: .5em;
                color: #363636;
                display: block;
                font-size: 1rem;
            }
            
            input, textarea{
                font-size:18px;
                display: block;
                max-width: 100%;
                min-width: 100%;
                padding: .625em;
                resize: vertical;
            }
            
            .select{
                display: inline-block;
                max-width: 100%;
                position: relative;
                vertical-align: top;
            }
            .select select{
                background-color: #fff;
                border-color: #dbdbdb;
                color: #363636;
                cursor: pointer;
                display: block;
                font-size: 1em;
                max-width: 100%;
                outline: 0;
                border-radius: 4px;
                height: 2em;
                line-height: 1.5;
            }
            
            .field input, .field select, .field textarea{
                
            }

        `
  ]
})
export class ModalComponent implements OnInit {
  @Input() modalId: string;

  amount: string = '100';
  currency: string = 'RUB';
  from: string = 'Maxim';
  date: string = '12.12.12';
  type: string = 'Type example';
  direction: string = 'incoming';
  description: string = 'description';

  constructor(
    private dataService: DataService
  ) {

  }

  ngOnInit() {
    window.onclick = (e) => {
      if (e.target === document.getElementById(this.modalId)) {
        this.closeModal();
      }
    };

  }

  showModal() {
    document.getElementById(this.modalId).style.display = 'block';
  }

  closeModal() {
    document.getElementById(this.modalId).removeAttribute('style');
  }

  submitForm() {
    this.dataService.createTransaction(
      {
        amount: this.amount,
        currency: this.currency,
        description: this.description,
        from: this.from,
        date: this.date,
        direction: this.direction,
        type: this.type
      });
    this.closeModal();
    this.amount = this.type = this.direction = this.description = this.currency = this.date = this.from = '';
  }
}
